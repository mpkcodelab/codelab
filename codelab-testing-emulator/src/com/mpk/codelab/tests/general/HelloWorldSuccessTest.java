package com.mpk.codelab.tests.general;

import static org.junit.Assert.*;

import org.junit.Test;

public class HelloWorldSuccessTest {

	@Test
	public void test() {
		final String helloWorld = "HelloWorld";
		assertEquals("HelloWorld", helloWorld);
	}
}
