package com.mpk.codelab.tests.general;

import static org.junit.Assert.*;

import org.junit.Test;

public class HelloWorldFailTest {

	@Test
	public void test() {
		final String helloWorld = "HelloWorld";
		assertNotEquals("HelloWorld", helloWorld + "x");
	}
}
